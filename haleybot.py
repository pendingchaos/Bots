# -*- coding: utf-8 -*-
from botlib import *
from botlib.testmsg import fuzzy_equal
from secrets import *
import calendar
import time
import os
import sys
import random
import string
import xkcd
import codecs
import traceback
import inspect
import requests

timefn = lambda m, c: time.strftime('%A %B %-d %-H:%M %Y (%Z)')

SMALLTALK = ['morning', 'evening', 'afternoon', 'good night', 'night night', 'night',
             'hi', 'hello', 'hai', 'hey', 'bye', 'goodbye', 'howdy', 'hiya']

UFLAG_OWNER = 'owner'
UFLAG_FOLLOWER = 'follower'

def bday(user, month, day):
    return ('happy birthday, %s'%user, lambda t, dayno: t.tm_mon==month and t.tm_mday==day)

HOLIDAYS = {'Christmas': ('merry christmas', lambda t, dayno: t.tm_mon==12 and t.tm_mday==25),
            'Christmas Eve': ('happy christmas eve', lambda t, dayno: t.tm_mon==12 and t.tm_mday==24),
            'Christmas Eve Eve': ('happy christmas eve eve', lambda t, dayno: t.tm_mon==12 and t.tm_mday==23),
            'Boxing Day': ('happy boxing day', lambda t, dayno: t.tm_mon==12 and t.tm_mday==26),
            'New Year\'s Day': ('happy new year', lambda t, dayno: t.tm_mon==1 and t.tm_mday==1),
            'Halloween': ('happy halloween', lambda t, dayno: t.tm_mon==10 and t.tm_mday==31),
            'pendingchaos\'s Birthday': bday('<@U076V80DA>', 2, 16),
            'alexoleary\'s Birthday': bday('<@U02EPLD5X>', 9, 10),
            'michcioperz\'s Birthday': bday('<@U02EBMATM>', 9, 29),
            '1500000000': ('Happy ~1,500,000,000', lambda t, dayno: abs(time.mktime(t)-1500000000)<5)}

last_exception = None

class HaleyBot(Bot):
    def __init__(self):
        Bot.__init__(self, 'haley', TOKEN_HALEY)
        def simple_response(c, r, p=1):
            self.responses[p][self.testfn(c)] = lambda m, c: r
        self.mood = self.get_bot_storage().get('mood', 100)
        self.responses[1][lambda m, _: all([c=='^' for c in m]) and self.get_mood()>=0] = lambda m, c: m+'^'
        self.responses[1][self.testfn('time')] = timefn
        self.responses[1][self.testfn('date')] = timefn
        self.responses[1][self.testfn('what is the time')] = timefn
        self.responses[1][self.testfn('what is the date')] = timefn
        self.responses[1][self.testfn('what\'s the time')] = timefn
        self.responses[1][self.testfn('what\'s the date')] = timefn
        self.responses[1][self.testfn('thyme')] = timefn
        self.responses[1][self.testfn('what is the thyme')] = timefn
        self.responses[1][self.testfn('respond', True)] = lambda m, c: self.randomstr()
        self.responses[1][self.testfn('how are you', True)] =\
            lambda m, c: 'I\'m fine' if self.get_mood() < 0 else 'I\'m alive!'
        self.responses[1][self.smalltalktestfn] = self.smalltalkresponsefn
        simple_response('brb', '_nods_')
        simple_response('gtg', '_nods_')
        simple_response('afk', '_nods_')
        simple_response('back', '_nods_')
        simple_response('boop', 'beep')
        simple_response('beep', 'boop')
        simple_response('ping', 'pong')
        simple_response('pong', 'ping')
        simple_response('_boops_', '_beeps_', 2)
        simple_response('_beeps_', '_boops_', 2)
        simple_response('_pings_', '_pongs_', 2)
        simple_response('_pongs_', '_pings_', 2)
        self.responses[1][self.testfn('thanks', True)] =\
            lambda m, c: 'you\'re welcome' if self.get_mood() < 0 else 'You\'re welcome!'
        self.responses[1][self.testfn('get me the latest xkcd', True)] = lambda m, c: xkcd.getLatestComic().link
        self.responses[1][self.testfn('get me a xkcd', True)] = lambda m, c: xkcd.getRandomComic().link
        self.responses[1][self.testfn('get me the latest whatif', True)] = lambda m, c: xkcd.getLatestWhatIf().link
        self.responses[1][self.testfn('get me a whatif', True)] = lambda m, c: xkcd.getRandomWhatIf().link
        self.responses[1][self.testfn('is anyone here?')] =\
            lambda m, c: 'I\'m here.' if self.get_mood() < 0 else 'I\'m here!'
        self.responses[1][self.testfn('anyone here?')] =\
            lambda m, c: 'I\'m here.' if self.get_mood() < 0 else 'I\'m here!'
        self.responses[2][self.testfn('help', True)] = lambda m, c: 'How may I help you?'
        self.responses[1][self.testfn('what are you doing', True)] = self.random_activity
        self.responses[1][self.testfn('what have you been doing', True)] = self.random_activity
        self.responses[1][self.testfn('what have you been up to', True)] = self.random_activity
        self.responses[1][self.testfn('whatcha doing', True)] = self.random_activity
        self.responses[1][self.testfn('watcha doing', True)] = self.random_activity
        self.responses[1][self.testfn('wassup', True)] = self.random_activity
        self.responses[1][self.testfn('how much wood would a woodchuck chuck if a wood chuck could chuck wood')] =\
            lambda m, c: 'According to https:///www.news.cornell.edu/stories/1996/02/groundhog-day-facts-and-factoids, about 700 pounds of wood'
        self.responses[1][self.positivetestfn('fizz')] = lambda m, c: 'buzz'
        self.responses[1][self.positivetestfn('buzz')] = lambda m, c: 'Have a banaana!'
        #self.responses[0][lambda m, c: random.randrange(50)==0 and self.get_mood()>=0] = lambda m, c: self.random_message()
        self.responses[0][lambda m, c: self.user_name in m and self.get_mood()>=0] =\
            lambda m, c: random.choice(['Was I summoned?', 'Who\'s %s?'%self.user_name])
        self.responses[1][self.testfn('what\'s your mood?', True)] = lambda m, c: self.get_mood_msg()
        self.responses[1][self.testfn('become ultra happy', True)] = lambda m, c: (self.set_mood(85), self.get_mood_msg())[1]
        self.responses[1][self.testfn('become happy', True)] = lambda m, c: (self.set_mood(60), self.get_mood_msg())[1]
        self.responses[1][self.testfn('become well', True)] = lambda m, c: (self.set_mood(35), self.get_mood_msg())[1]
        self.responses[1][self.testfn('become normal', True)] = lambda m, c: (self.set_mood(0), self.get_mood_msg())[1]
        self.responses[1][self.testfn('become melancholy', True)] = lambda m, c: (self.set_mood(-85), self.get_mood_msg())[1]
        self.responses[1][self.testfn('become sad', True)] = lambda m, c: (self.set_mood(-60), self.get_mood_msg())[1]
        self.responses[1][self.testfn('become a little sad', True)] = lambda m, c: (self.set_mood(-35), self.get_mood_msg())[1]
        self.responses[1][self.testfn('become awesone', True)] = lambda m, c: 'sorry, I\'m already awesome :/'
        self.responses[1][self.testfn('restart', True)] = self.restart
        for user, id in self.user_name_to_id.iteritems():
            def tf(m, c):
                for user in self.user_name_to_id.keys():
                    if m == 'what\'s the id of %s, haley' % user:
                        return True
            def rf(m, c):
                for user, id in self.user_name_to_id.iteritems():
                    if m == 'what\'s the id of %s, haley' % user:
                        return 'The id of %s is %s' % (user, id)
            self.responses[1][tf] = rf
        self.user_presences = self.get_bot_storage().get('user_presences', {})
    
    def wrap_callback(self, cb):
        def f(*args, **kwargs):
            try:
                return cb(*args, **kwargs)
            except Exception as e:
                self.send_to_users('Exception: %s'%post_to_hastebin(traceback.format_exc()), [UFLAG_OWNER])
                raise e
        return Bot.wrap_callback(self, f)
    
    def restart(self, m, c):
        self.send_message(c, 'Restarting...')
        os.environ['RESTART_CHANNEL'] = c
        self.send_to_users('Restarting!', [UFLAG_FOLLOWER])
        self.ws.close()
    
    def testfn(self, s, direct_req=False):
        def f(m, c):
            s2 = s.format(**{k:getattr(self, k) for k in dir(self)}).lower()
            return testmsg(m, s, direct_req, self.user_name)
        return f
    
    def positivetestfn(self, s, direct_req=False):
        f = self.testfn(s, direct_req)
        return lambda m, c: f(m, c) and self.get_mood()>=0
    
    def smalltalktestfn(self, msg, c):
        msg = msg.lower()
        for s in SMALLTALK:
            if self.testfn(s)(msg, c): return True
        return False
    
    def smalltalkresponsefn(self, msg, c):
        msg = msg.lower()
        for s in SMALLTALK:
            if self.testfn(s)(msg, c): return s[0].upper() + s[1:] + ('!' if self.mood>=0 else '')
        return False
    
    def get_mood(self): # Used in closures
        return self.mood
    
    def get_mood_msg(self):
        if self.mood > 75: return 'I\'m ultra happy! (%d)' % self.mood
        elif self.mood > 50: return 'I\'m happy! (%d)' % self.mood
        elif self.mood > 25: return 'I\'m well (%d)' % self.mood
        elif self.mood < -75: return 'I\'m melancholy (%d)' % self.mood
        elif self.mood < -50: return 'I\'m sad (%d)' % self.mood
        elif self.mood < -25: return 'I\'m a little sad (%d)' % self.mood
        else: return 'I\'m normal (%d)' % self.mood
    
    def set_mood(self, mood):
        mood = min(max(mood, -100), 100)
        storage = self.get_bot_storage()
        storage['mood'] = mood
        changed = mood != self.mood
        self.mood = mood
        self.set_bot_storage(storage)
        if changed: self.send_to_users(self.get_mood_msg(), [UFLAG_FOLLOWER])
    
    def randomstr(self):
        chars = string.ascii_lowercase
        num = random.randint(35, 60)
        return ''.join(random.choice(chars) for i in range(num))
    
    def on_open(self, ws):
        global last_exception
        
        Bot.on_open(self, ws)
        
        if 'RESTART_CHANNEL' in os.environ:
            self.send_message(os.environ['RESTART_CHANNEL'], 'Restarted!')
            del os.environ['RESTART_CHANNEL']
            print "Restarted!"
        
        self.general_channel = [c['id'] for c in self.team_info['channels'] if c['is_general']][0]
        
        t = calendar.timegm(time.gmtime())
        presences = {user['id']:(user['name'], t) for user in self.team_info['users'] if user['presence']!='away'}
        self.on_became_active(presences)
        
        #self.send_to_users('Haley bot activated! :D', [UFLAG_FOLLOWER])
        
        if last_exception:
            self.send_to_users('Last exception: %s'%post_to_hastebin(last_exception), [UFLAG_OWNER])
            last_exception = None
    
    def on_close(self, ws):
        Bot.on_close(self, ws)
        #self.send_to_users('Haley bot deactivated! D:', [UFLAG_FOLLOWER])
    
    def on_error(self, ws, err):
        Bot.on_error(self, ws, err)
        self.send_to_users('Error! D: %s'%post_to_hastebin(err), [UFLAG_OWNER])
    
    def on_became_active(self, presences):
        TWO_WEEKS = 1209600
        for k, v in presences.iteritems():
            if v[1]-self.user_presences.get(k, (None, 0))[1] >= TWO_WEEKS:
                if self.get_mood() >= 0:
                    self.send_message(self.general_channel, '<@%s> is alive!' % k)
                else:
                    self.send_message(self.general_channel, 'hi <@%s>' % k)
            self.user_presences[k] = v
        storage = self.get_bot_storage()
        storage['user_presences'] = self.user_presences
        self.set_bot_storage(storage)
    
    def on_became_away(self, presences):
        for k, v in presences.iteritems(): self.user_presences[k] = v
        storage = self.get_bot_storage()
        storage['user_presences'] = self.user_presences
        self.set_bot_storage(storage)
    
    def on_interval(self, interval):
        storage = self.get_bot_storage()
        holidays = storage.get('holidays', {})
        t = time.gmtime()
        dayno = (t.tm_mday-1)//7 + 1
        for name, holiday in HOLIDAYS.iteritems():
            if holiday[1](t, dayno):
                last_celebrated = holidays.get(name, {}).get('last_celebrated', 0)
                if time.strftime("%Y-%j", t) == last_celebrated:
                    continue
                holidayobj = holidays.get(name, {})
                holidayobj['last_celebrated'] = time.strftime("%Y-%j", t)
                holidays[name] = holidayobj
                channel = self.general_channel
                self.send_message(channel, holiday[0] + '!' if self.get_mood()>0 else '')
        storage['holidays'] = holidays
        self.set_bot_storage(storage)
    
    def random_message(self):
        messages = ['hahahaha', 'heh', 'nice', 'cool', 'LOL', 'yeah right', 'ok', '_nods_',
                    'Wait what?', 'Wat', 'AHAHAHAHAHAHAHA', 'riight', 'OMG', ':P', ':0',
                    ':)', 'D:', ':D', ':(', 'Oooooooh', 'You said a bad word! D:', '¯\_(ツ)_/¯']
        return random.choice(messages)
    
    def on_event(self, ws, event):
        if event.get('type', None) == 'presence_change' and event['presence'] == 'active':
            username = [u['name'] for u in self.team_info['users'] if u['id'] == event['user']][0]
            self.on_became_active({event['user']: (username, calendar.timegm(time.gmtime()))})
        elif event.get('type', None) == 'presence_change' and event['presence'] == 'away':
            username = [u['name'] for u in self.team_info['users'] if u['id'] == event['user']][0]
            self.on_became_away({event['user']: (username, calendar.timegm(time.gmtime()))})
        Bot.on_event(self, ws, event)
    
    def random_activity(self, message):
        if self.mood >= 0:
            messages = ['Eating loads of cake!', 'Not much', 'Fixing my submarine',
                        'Breaking my submarine', 'Making loads of cake!', 'Lots of stuff']
            return random.choice(messages)
        else:
            return 'nothing much'
    
    def add_word(self, word, diff):
        storage = self.get_bot_storage()
        if 'words' in storage: storage['words'][word] = diff
        else: storage['words'] = {word: diff}
        self.set_bot_storage(storage)
    
    def remove_word(self, word):
        storage = self.get_bot_storage()
        del storage.get('words', {})[word]
        self.set_bot_storage(storage)
    
    def should_respond(self, event):
        if not Bot.should_respond(self, event): return False
        
        text = event.get('text', '')
        
        # TODO: This should be somewhere else
        if text.startswith('haley, add word:'):
            word = text[16:].lstrip().rstrip()
            diff_str = word[::-1].split()[0][::-1]
            word = word[:-len(diff_str)].lstrip().rstrip()
            self.add_word(word, int(diff_str))
            self.send_message(event['channel'], '\'%s\' added to the word list' % word)
            return False
        elif text.startswith('haley, del word:'):
            word = text[16:].lstrip().rstrip()
            try:
                self.remove_word(word)
                self.send_message(event['channel'], '\'%s\' removed from the word list' % word)
            except KeyError:
                self.send_message(event['channel'], '\'%s\' was not in the word list' % word)
            return False
        elif testmsg(text, 'what is the word list', True, self.user_name):
            words = self.get_bot_storage().get('words', [])
            s = ''
            for word, diff in words.iteritems():
                if diff < 0: s += '%s -= %d\n' % (word, abs(diff))
                else: s += '%s += %d\n' % (word, diff)
            self.send_message(event['channel'], 'Here you go :D '+post_to_hastebin(s))
            return False
        elif text.startswith('haley, say in '):
            rest = text[14:].lstrip()
            channel = rest[:rest.find('>')].split('|')[0][2:]
            text = rest[rest.find('>')+1:]
            if channel in [c['id'] for c in self.team_info['channels']]:
                self.send_message(event['channel'], 'Sending message! :D')
                self.send_message(channel, text)
            else:
                self.send_message(event['channel'], 'No such channel! D:')
            return False
        
        words = self.get_bot_storage().get('words', {})
        for word, diff in words.iteritems():
            for i in range(len(text)-len(word)+1):
                if fuzzy_equal(word, text[i:i+len(word)]): self.set_mood(self.mood+diff)
        
        return True

if __name__ == '__main__':
    while True:
        bot = None
        try:
            bot = HaleyBot()
            bot.run()
        except Exception as e:
            if isinstance(e, SystemExit): raise e
            elif isinstance(e, KeyboardInterrupt): raise e
            elif isinstance(e, requests.exceptions.HTTPError) and str(e).startswith('429 Client Error'):
                print traceback.format_exc()
                time.sleep(61)
            else:
                print traceback.format_exc()
                last_exception = traceback.format_exc()
                time.sleep(5)
        
        if 'RESTART_CHANNEL' in os.environ:
            print "Restarting..."
            args = [s.replace(' ', '\ ') for s in sys.argv]
            args[0] = inspect.stack()[0][1]
            try:
                #TODO: Flush files
                sys.stdout.flush()
                sys.stderr.flush()
                os.execve('/usr/bin/python2', ['python2']+args, dict(os.environ))
            except OSError:
                print traceback.format_exc()
                last_exceptions = traceback.format_exc()
                time.sleep(5)
