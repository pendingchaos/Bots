def fuzzy_equal(s, ref):
    if abs(len(ref)-len(s)) > 4: return False
    found = 0
    found_near = 0
    stri = 0
    refi = 0
    while stri<len(s) and refi<len(ref):
        if s[stri]==ref[refi]:
            found += 1
            stri += 1
            refi += 1
            continue
        
        near = []
        for off in range(-1, 2):
            if stri+off >= 0 and stri+off < len(s): near.append(s[stri+off]) 
        
        if ref[refi] in near:
            stri = near.index(ref[refi])-1 + stri
            s = s[:stri] + s[stri+1:]
            found_near += 1
            refi += 1
            continue
        
        refi += 1
    
    near_thresh = len(ref) / 4 # 4
    discard_thresh = len(ref) / 4 # 2
    thresh = min(max(4.0 / len(ref), 0.8), 1) #0.8
    
    discarded = max(len(ref), len(s)) - found - found_near
    
    if found_near > near_thresh: return False
    if discarded > discard_thresh: return False
    return (found+found_near)/float(len(ref)) >= thresh

def preprocess_msg(msg):
    msg = msg.lower()
    remove = [' ', '\t', '\n', "'", '-', ',', ':D', 'D:', ':)', ':(', '(:', '):', '^_^',
              ':P', ':3', '-.-', '._.', '=)', '(=', '8)', '(8']
    for s in remove: msg = msg.replace(s, '')
    while len(msg) and msg[-1] in '?!.:;': msg = msg[:-1]
    return msg

def fuzzy_endswith(s, ref):
    s = s[::-1]
    ref = ref[::-1]
    s = s[:min(len(s), len(ref))]
    if fuzzy_equal(s, ref): return len(ref)
    return -1

def fuzzy_startswith(s, ref):
    s = s[:min(len(s), len(ref))]
    if fuzzy_equal(s, ref): return len(ref)
    return -1

def is_directed_at_user(msg, user):
    if fuzzy_endswith(msg, '@'+user) >= 0: return True
    if fuzzy_startswith(msg, '@'+user+':') >= 0: return True
    if fuzzy_startswith(msg, '@'+user+',') >= 0: return True
    if fuzzy_startswith(msg, user+',') >= 0: return True
    if fuzzy_endswith(msg, user) >= 0: return True
    if fuzzy_startswith(msg, user+':') >= 0: return True
    if fuzzy_endswith(msg, 'everyone') >= 0: return True
    if fuzzy_startswith(msg, 'everyone:') >= 0: return True
    if fuzzy_endswith(msg, 'here') >= 0: return True
    if fuzzy_startswith(msg, 'here:') >= 0: return True
    return False

def further_process_msg(msg, user):
    if fuzzy_endswith(msg, '@'+user) >= 0: msg = msg[:-fuzzy_endswith(msg, '@'+user)]
    elif fuzzy_startswith(msg, '@'+user+':') >= 0: msg = msg[fuzzy_startswith(msg, '@'+user+':'):]
    elif fuzzy_startswith(msg, '@'+user+',') >= 0: msg = msg[fuzzy_startswith(msg, '@'+user+','):]
    elif fuzzy_startswith(msg, user+',') >= 0: msg = msg[fuzzy_startswith(msg, user+','):]
    elif fuzzy_endswith(msg, user) >= 0: msg = msg[:-fuzzy_endswith(msg, user)]
    elif fuzzy_startswith(msg, user+':') >= 0: msg = msg[fuzzy_startswith(msg, user+':'):]
    elif fuzzy_endswith(msg, 'everyone') >= 0: msg = msg[:-fuzzy_endswith(msg, 'everyone')]
    elif fuzzy_startswith(msg, 'everyone:') >= 0: msg = msg[fuzzy_startswith(msg, 'everyone:'):]
    elif fuzzy_endswith(msg, 'here') >= 0: msg = msg[:-fuzzy_endswith(msg, 'here')]
    elif fuzzy_startswith(msg, 'here:') >= 0: msg = msg[fuzzy_startswith(msg, 'here:'):]
    return msg

def testmsg(msg, ref, direct_req, user):
    direct = is_directed_at_user(msg, user)
    origmsg = msg
    msg = preprocess_msg(msg)
    ref = preprocess_msg(ref)
    msg2 = preprocess_msg(further_process_msg(origmsg, user))
    if direct_req and not direct: return False
    if fuzzy_equal(msg2, ref): return True
    if fuzzy_equal(msg, ref): return True
    return False

if __name__ == '__main__':
    while True:
        msg = raw_input('> ')
        if testmsg(msg, 'hi', False, 'haley'): print 'Hi!'
        if testmsg(msg, 'hello', False, 'haley'): print 'Hello!'
        if testmsg(msg, 'howareyou', True, 'haley'): print 'I\'m alive!'
        if testmsg(msg, 'whatsthetime', True, 'haley'): print 'It\'s sometime \o/'
