from slacker import Slacker
from . import websocket
from atomicwrites import atomic_write
import json
import time
import hashlib
import os.path
from testmsg import testmsg
import htmllib
import httplib
import codecs
import threading

NUM_PRIORITIES = 255
INTERVAL = 1

#TODO: More could probably be added
def unescape(s):
    s = s.replace('&lt;', '<').replace('&gt;', '>').replace('&quot;', '"')
    s = s.replace('&apos;', '\'').replace('&amp;', '&')
    return s

def post_to_hastebin(text):
    c = httplib.HTTPConnection('hastebin.com')
    c.request('POST', '/documents', codecs.encode(text, 'utf-8'))
    url = 'hastebin.com/' + json.loads(c.getresponse().read())['key']
    c.close()
    return url

class Bot(object):
    def __init__(self, name, token):
        time.sleep(0.25) #To help prevent hitting of rate limits
        
        print '%s bot here!\n' % name
        
        self.storage_lock = threading.Lock()
        
        self.botid = '%s_%s' % (name, hashlib.sha512(token).hexdigest())
        self.storage = None
        self.storage = self.get_bot_storage()
        
        self.slackapi = Slacker(token)
        res = self.slackapi.rtm.start()
        if not res.body['ok']:
            print res.body['error']
            raise Exception
        elif 'warning' in res.body:
            print res.body['warning']
        
        self.user_flags = self.get_bot_storage().get('user_flags', {})
        
        self.team_info = None
        self.user_name_to_id = None
        self.user_id_to_name = None
        self.user_id = None
        self.user_name = None
        self.on_init(res.body)
        
        self.enabled = self.get_bot_storage().get('enabled', True)
        self.responses = {i:{} for i in range(NUM_PRIORITIES)}
        
        websocket.enableTrace(True)
        self.ws = websocket.WebSocketApp(res.body['url'],
                                         on_message=self.wrap_callback(lambda ws, msg:
                                                                           self.on_event(ws, json.loads(msg))),
                                         on_error=self.wrap_callback(lambda ws, error: self.on_error(ws, error)),
                                         on_close=self.wrap_callback(self.on_close),
                                         on_interval=lambda ws, i: self.on_interval(i),
                                         interval=INTERVAL)
        self.ws.on_open = self.on_open
    
    def wrap_callback(self, cb):
        return cb
    
    def get_bot_storage(self):
        if self.storage: return json.loads(json.dumps(self.storage))
        
        try:
            f = open(os.path.join(os.path.expanduser('~'), '.bots/%s' % self.botid))
            return json.loads(f.read())
        except IOError:
            return {}
    
    def set_bot_storage(self, data):
        if data == self.storage: return
        fname = os.path.join(os.path.expanduser('~'), '.bots/%s' % self.botid)
        with atomic_write(fname, overwrite=True) as f:
            f.write(json.dumps(data, indent=4))
        self.storage = json.loads(json.dumps(data))
    
    def add_user_flag(self, user_name, flag):
        if user_name in self.user_name_to_id:
            l = set(self.user_flags.get(self.user_name_to_id[user_name], ('', []))[1])
            l.add(flag)
            self.user_flags[self.user_name_to_id[user_name]] = (user_name, list(l))
            storage = self.get_bot_storage()
            storage['user_flags'] = self.user_flags
            self.set_bot_storage(storage)
    
    def del_user_flag(self, user_name, flag):
        if user_name in self.user_name_to_id:
            l = set(self.user_flags.get(self.user_name_to_id[user_name], ('', []))[1])
            try: l.remove(flag)
            except KeyError: pass
            self.user_flags[self.user_name_to_id[user_name]] = (user_name, list(l))
            storage = self.get_bot_storage()
            storage['user_flags'] = self.user_flags
            self.set_bot_storage(storage)
    
    def run(self):
        self.ws.run_forever()
    
    def send_to_users(self, msg, req_flags):
        for user_id in self.user_id_to_name.keys():
            if user_id == self.user_id: continue
            if set(req_flags).issubset(set(self.user_flags.get(user_id, ('',[]))[1])):
                channel = [im['id'] for im in self.team_info['ims'] if im['user']==user_id]
                self.send_message(channel[0], msg)
    
    def on_event(self, ws, event):
        if 'text' in event and event.get('type', None)=='message':
            message = event['text']
            for user_id, user_name in self.user_id_to_name.iteritems():
                message = message.replace('<@%s>'%user_id, '@'+user_name)
            message = message.replace('<!here|@here>', '@here')
            message = unescape(message)
            event['text'] = message
        
        if not self.should_respond(event): return
        
        time.sleep(0.25)
        
        channel = event['channel']
        
        if self.testfn('become disabled', True)(message, channel):
            response = 'Becoming disabled.' if self.enabled else 'I\'m already disabled.'
            self.send_message(event['channel'], response)
            self.set_enabled(False)
            return
        elif self.testfn('become enabled', True)(message, channel):
            response = 'I\'m already enabled.' if self.enabled else 'Becoming enabled.'
            self.send_message(event['channel'], response)
            self.set_enabled(True)
            return
        elif self.testfn('are you enabled?', True)(message, channel):
            response = 'I\'m enabled.' if self.enabled else 'I\'m not enabled.'
            self.send_message(event['channel'], response)
            return
        else:
            if not self.enabled: return
        
        for p in reversed(range(NUM_PRIORITIES)):
            for test, get_response in self.responses[p].iteritems():
                if test(message, channel):
                    response = get_response(message, channel)
                    if response: self.send_message(channel, response)
                    return
        
        self.on_unknown_message(message, event)
    
    def set_enabled(self, enabled):
        storage = self.get_bot_storage()
        storage['enabled'] = enabled
        self.enabled = enabled
        self.set_bot_storage(storage)
    
    def on_unknown_message(self, message, event):
        pass
    
    def send_message(self, channel_id, message):
        for user_id, user_name in self.user_id_to_name.iteritems():
            message = message.replace('@'+user_name, '<@%s>'%user_id)
        message = message.replace('@here', '<!here|@here>')
        self.ws.send(json.dumps({'type': 'message',
                                 'channel': channel_id,
                                 'user': self.user_id,
                                 'text': message}))
    
    def on_error(self, ws, err):
        print err
    
    def on_open(self, ws):
        pass
    
    def on_close(self, ws):
        pass
    
    def on_init(self, res):
        self.team_info = res
        self.user_name_to_id = {user['name']:user['id'] for user in self.team_info['users']}
        self.user_id_to_name = {user['id']:user['name'] for user in self.team_info['users']}
        self.user_id = self.team_info['self']['id']
        self.user_name = self.team_info['self']['name']
    
    def on_interval(self, interval):
        pass
    
    def should_respond(self, event):
        if event.get('type', None) != 'message': return False
        if event.get('user', None) == self.user_id: return False
        if 'text' not in event: return False
        return True
